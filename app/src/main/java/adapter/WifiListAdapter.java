package adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.yyl.wifi_send.R;

import java.util.List;

/**
 * Created by 40503 on 2017/12/26.
 */

public class WifiListAdapter extends BaseAdapter {

    private Context mContext;
    private List<ScanResult> results;

    public WifiListAdapter(Context mContext, List<ScanResult> results) {
        this.mContext = mContext;
        this.results = results;
    }

    @Override
    public int getCount() {
        return results == null ? 0 : results.size();
    }

    @Override
    public Object getItem(int i) {
        return results.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_item_device, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_device);
        ScanResult sr = results.get(i);
        tv.setText(TextUtils.isEmpty(sr.SSID) ? "Unkown" : sr.SSID);
        return v;
    }

    public void setData(List<ScanResult> results) {
        this.results = results;
        notifyDataSetChanged();
    }
}
