package com.yyl.wifi_send;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;


public class ToastUtil {
    private static boolean isDebug = true;

    public static void show(String s, Context context) {
        showShort(s, context);
    }

    public static void showShort(String s, Context context) {
        Toast toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 150);
        toast.show();
    }

    public static void showLong(String s, Context context) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }
}
