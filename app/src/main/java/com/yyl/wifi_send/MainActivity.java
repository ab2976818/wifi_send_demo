package com.yyl.wifi_send;

import android.Manifest;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.yyl.wifi_manage.WifiConnectManage;
import com.yyl.wifi_manage.bean.WifiBaseInfo;
import com.yyl.wifi_manage.event.WifiManageStatus;

import java.util.List;

import adapter.WifiListAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;

public class MainActivity extends BaseActivity implements WifiManageStatus, AdapterView.OnItemSelectedListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wifi_lable)
    TextView wifiLable;
    @BindView(R.id.spinner)
    AppCompatSpinner spinner;
    @BindView(R.id.spinner_rl)
    RelativeLayout spinnerRl;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.pwd_dispaly)
    CheckBox pwdDispaly;
    @BindView(R.id.text_hint)
    TextView textHint;
    @BindView(R.id.input_rl)
    RelativeLayout inputRl;
    @BindView(R.id.next)
    Button next;
    @BindView(R.id.send_input)
    EditText sendInput;
    @BindView(R.id.send_btn)
    Button sendBtn;
    @BindView(R.id.send_ll)
    LinearLayout sendLl;
    @BindView(R.id.back_url)
    TextView backUrl;
    private WifiConnectManage connectManage;
    private WifiListAdapter adapter;

    private List<ScanResult> results;
    private ScanResult selectResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        if (!aBoolean) {
                            ToastUtil.show(getString(R.string.Please_check_your_permissions), MainActivity.this);
                        }
                    }
                });
        initView();
    }

    @Override
    protected int getActivityRId() {
        return R.layout.activity_main;
    }

    private void initView() {
        adapter = new WifiListAdapter(this, results);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        pwdDispaly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        connectManage = new WifiConnectManage(this, this);
        connectManage.setManualDirective(true);
        initScan();
    }

    private void initScan() {
        inputRl.setVisibility(View.VISIBLE);
        sendLl.setVisibility(View.GONE);
        connectManage.getScanResult().subscribe(new Action1<List<ScanResult>>() {
            @Override
            public void call(List<ScanResult> scanResults) {
                initData(scanResults);
            }
        });
    }

    private void initData(List<ScanResult> resultList) {
        if (resultList != null && resultList.size() > 0) {
            results = resultList;
            if (adapter != null) {
                adapter.setData(results);
            }
            selectResults = resultList.get(0);
        }
    }

    public void showToastMessage(final int msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToastUtil.show(getString(msg), MainActivity.this);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectResults = results.get(position);
        connectManage.wifiIs(results.get(position))
                .subscribe(new Action1<WifiBaseInfo>() {
                    @Override
                    public void call(WifiBaseInfo info) {
                        if (info.isHasPassword()) {
                            password.setVisibility(View.VISIBLE);
                            pwdDispaly.setVisibility(View.VISIBLE);
                        } else {
                            password.setVisibility(View.GONE);
                            pwdDispaly.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void operationError(int errorCode) {
        dismissProgressDialog();
        switch (errorCode) {
            case WifiConnectManage.CONNECT_FAIL:
                showToastMessage(R.string.wifi_disabled);
                break;
            case WifiConnectManage.PASSWORD_ERROR:
                showToastMessage(R.string.invalid_pwd);
                break;
            case WifiConnectManage.NOT_DEVICE:
                showToastMessage(R.string.no_device);
                break;
            case WifiConnectManage.NOT_RESPONSE:
                showToastMessage(R.string.not_response);
                break;
            case WifiConnectManage.SETTING_FAIL:
                showToastMessage(R.string.setting_failure);
                break;
        }

    }

    @Override
    public void deviceBindSuccess(String bindDevice) {
        if (TextUtils.isEmpty(bindDevice)) {
            inputRl.setVisibility(View.GONE);
            sendLl.setVisibility(View.VISIBLE);
        } else {
            backUrl.setText("当前返回的地址为：" + bindDevice);
        }
        dismissProgressDialog();
    }

    @Override
    public void orderBack(final String order) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String str=backUrl.getText().toString()+order+"\n";
                backUrl.setText("");
                backUrl.setText(str);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            initScan();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @OnClick({R.id.send_btn, R.id.next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.send_btn:
                if (TextUtils.isEmpty(sendInput.getText().toString())) {
                    ToastUtil.show("指令不能为空", this);
                    return;
                }
                showProgressDialog();
                connectManage.sendDirective(sendInput.getText().toString());
                break;
            case R.id.next:
                if (password.getVisibility() == View.VISIBLE) {
                    String pwd = password.getText().toString();
                    if (TextUtils.isEmpty(pwd)) {
                        showToastMessage(R.string.pwd_invalid_1);
                        return;
                    }
                    if (pwd.length() < 8) {
                        showToastMessage(R.string.pwd_invalid_2);
                        return;
                    }
                }
                showProgressDialog();
                connectManage.startConnect(selectResults, password.getText().toString())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean aBoolean) {
                                if (aBoolean) {
                                    connectManage.initConnectDeviceWifi(null);
                                }
                            }

                        });
                break;
        }
    }
}
