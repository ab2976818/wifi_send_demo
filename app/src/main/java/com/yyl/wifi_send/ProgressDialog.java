package com.yyl.wifi_send;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;

import com.tuyenmonkey.mkloader.MKLoader;


/**
 * Created by yang on 2016/8/12.
 */
public class ProgressDialog extends Dialog {

    private Context context;
    private static ProgressDialog customMyProgressDialog = null;

    public ProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    public ProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public static ProgressDialog createDialog(Context context) {
        customMyProgressDialog = new ProgressDialog(context, R.style.CustomProgressDialog);
        customMyProgressDialog.setContentView(R.layout.progress_dialog);
        customMyProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        return customMyProgressDialog;
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (customMyProgressDialog == null) {
            return;
        }
        MKLoader mkLoader = (MKLoader) customMyProgressDialog.findViewById(R.id.progress);
        mkLoader.setVisibility(View.VISIBLE);
//        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
//        animationDrawable.start();
    }


}
