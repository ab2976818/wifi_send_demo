package com.yyl.wifi_send;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Stack;

import butterknife.ButterKnife;

/**
 * Created by 90835 on 2017/2/28.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private ProgressDialog pd;
    protected Stack<Fragment> fragmentList;
    protected Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityRId());
        mHandler = new Handler();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0及以上
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4到5.0
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
        ButterKnife.bind(this);
        fragmentList = new Stack<>();
        initProgressDialog();
    }
    /**
     * 初始化加载框
     */
    public void initProgressDialog() {
        if (pd == null && this != null) {
            pd = ProgressDialog.createDialog(this);
            pd.setCancelable(false);

        }
    }

    /**
     * 显示加载框
     */
    public void showProgressDialog() {
        if (pd == null || this == null) return;
        if (!pd.isShowing()) {
            pd.show();
        }
    }

    /**
     * 隐藏
     */
    public void dismissProgressDialog() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

    public void back(View v) {
        onBackPressed();
    }

    protected abstract int getActivityRId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fragmentList.clear();
    }




}
